import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import { Container } from "@material-ui/core";
import { ProtectedRoute } from "./ProtectedRoute";

const Login = lazy(()=> import("./pages/auth/Login"))
const User = lazy(()=> import("./pages/user/User"))

function App() {
  return (
    <Router>
      <Switch>
        <Suspense fallback={<Container>Loading Suspense...</Container>}>
          <Route path="/login" component={Login} />
          <ProtectedRoute path="/user" component={User} />
          <Route exact path="/" component={Login} />
        </Suspense>
      </Switch>
    </Router>
  );
}

export default App;
