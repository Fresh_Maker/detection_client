import { Button, Chip, Container } from "@material-ui/core";
import { get_user_list } from "api/users_api";
import { I_Base, I_find_count } from "interfaces/common_interface";
import { I_enums, I_user } from "interfaces/database_interface";
import React, { useEffect, useMemo, useState } from "react";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";

export interface IUserProps extends  I_Base {}

const UserList: React.FunctionComponent<IUserProps> = (props: IUserProps): JSX.Element => {
  const [isLoading, setIsLoading] = useState(true);
  const [userList, setUserList] = useState([] as I_user[]);
  const [isHideDirector, setIsHideDirector] = useState(false);

  const columns = useMemo(
    () => [
      {
        name: "Name",
        selector: "name",
        sortable: true,
        cell: (row: any) => <Link to={`/users/${row.id}`}>{row.name}</Link>,
      },
      {
        name: "Email",
        selector: "email",
        sortable: true,
        omit: isHideDirector,
      },
      {
        name: "Roles",
        selector: "roles",
        cell: ({ roles }: { roles: I_enums[] }) => {
          const role_list = roles
            .sort((a, b) => {
              if (a.value < b.value) {
                return -1;
              }
              if (a.value > b.value) {
                return 1;
              }
              return 0;
            })
            .map((role: I_enums, index: number) => (
              <Chip key={index} label={role.value} />
            ));
          return <div className="role-list">{role_list}</div>;
        },
      },
    ],
    [isHideDirector]
  );

  useEffect(() => {
    setIsLoading(true);
    get_user_list().then((response: I_find_count<I_user>) => {
      setIsLoading(false);
      if (response.list) {
        setUserList(response.list);
      }
    });
  }, []);

  let html = <div>Loading user list...</div>;
  if (!isLoading) {
    html = (
      <div>
        <DataTable title="User List" columns={columns} data={userList} />
        <Button
          variant="contained"
          color="primary"
          onClick={() => setIsHideDirector(!isHideDirector)}
        >
          Hide Directory Column
        </Button>
      </div>
    );
  }

  return (<Container>{html}</Container>);
}

export default UserList;