import {
  Box,
  Button, Container,
  Grid, TextField
} from "@material-ui/core";
import { Color } from "@material-ui/lab/Alert";
import { get_enum_by_type_ } from "api/enums_api";
import { create_user, get_user_detail, update_user } from "api/users_api";
import MultiSelectJL from "components/form/MultiSelectJL";
import ToastMessage from "components/ToastMessage";
import { useFormik } from "formik";
import { I_response } from "interfaces/common_interface";
import { I_enums, I_user } from "interfaces/database_interface";
import * as React from "react";
import { useEffect, useState } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import * as yup from "yup";

const ids_to_enum_list = (number_list: number[], enum_list: I_enums[]) => {
  const result: I_enums[] = number_list.reduce((acc, number) => {
    const found_index = enum_list.findIndex((enums) => enums.id === number);
    if (found_index !== -1) {
      acc.push(enum_list[found_index]);
    }
    return acc;
  }, [] as I_enums[]);
  return result;
};
const enums_to_id_list = (enum_list: I_enums[]) => {
  const result: number[] = [];
  if (enum_list && enum_list.length === 0) {
    return result;
  }
  return enum_list.map((enums) => enums.id);
};
interface MatchParams {
  id: string;
}

export interface IUserDetailProps extends RouteComponentProps<MatchParams> {}

export default React.memo((props: IUserDetailProps) => {
  const user_id = props.match.params.id;
  const [isLoading, setIsLoading] = useState(true);
  const [userDetail, setUserDetail] = useState({
    name: "",
    email: "",
    password: "",
    roles: [] as I_enums[],
    organizations: [] as I_enums[],
    id: -1,
  } as I_user);
  const [isShowToast, setIsShowToast] = useState(false);
  const [isLoadError, setIsLoadError] = useState(false);
  const [loadErrorMessage, setLoadErrorMessage] = useState("");
  const [toastSeverity, setToastSeverity] = useState("success" as Color);
  const [toastMessage, setToastMessage] = useState("success");

  const [organizationList, setOrganizationList] = useState([] as I_enums[]);
  const [roleList, setRoleList] = useState([] as I_enums[]);

  useEffect(() => {
    setIsLoading(true);
    if (user_id !== undefined) {
      get_user_detail(user_id)
        .then((response: I_response<I_user>) => {
          setIsLoading(false);
          if (response.item) {
            setUserDetail(response.item);
          }
        })
        .catch(({ response }) => {
          setIsLoading(false);
          if (response.data.error) {
            setIsLoadError(true);
            setLoadErrorMessage(response.data.error);
          }
        });
    }
    get_enum_by_type_("USER_ROLE").then((response: I_enums[]) => {
      setRoleList(response);
      setIsLoading(false);
    });
    get_enum_by_type_("ORGANIZATION").then((response: I_enums[]) => {
      setOrganizationList(response);
      setIsLoading(false);
    });
  }, [user_id]);

  const validationSchema = yup.object({
    name: yup
      .string()
      .strict()
      .trim()
      .max(32, "Name should of maximum 32 characters length")
      .required("Name is required"),
    email: yup
      .string()
      .email()
      .trim()
      .min(3, "Email should of minimum 3 characters length")
      .max(32, "Email should of maximum 32 characters length")
      .required("Email is required"),
    password: yup
      .string()
      .min(3, "Password should of minimum 3 characters length")
      .required("Password is required"),
    roles: yup.array().of(yup.number()),
    organizations: yup.array().of(yup.number()),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: userDetail.id,
      name: userDetail.name,
      email: userDetail.email,
      password: userDetail.password,
      roles: enums_to_id_list(userDetail.roles),
      organizations: enums_to_id_list(userDetail.organizations),
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      let action_promise = null;
      let action_message = "";

      const tmp_userDetail = { ...userDetail };
      tmp_userDetail.name = values.name;
      tmp_userDetail.email = values.email;
      tmp_userDetail.password = values.password;
      tmp_userDetail.roles = ids_to_enum_list(values.roles, roleList);
      tmp_userDetail.organizations = ids_to_enum_list(
        values.organizations,
        organizationList
      );
      if (values.id === -1) {
        action_promise = create_user(tmp_userDetail);
        action_message = "Create";
      } else {
        action_promise = update_user(tmp_userDetail);
        action_message = "Update";
      }

      action_promise.then((response: I_response<I_user>) => {
        if (response.error) {
          setToastSeverity("error");
          setToastMessage(action_message + ": " + response.error.message);
        } else {
          setToastSeverity("success");
          setToastMessage(action_message + ": success");
        }
        setIsShowToast(true);
      });
    },
  });

  let form = <div>loading user form...</div>;
  let error_message = <div id="error_message"></div>;
  if (!isLoading && !isLoadError) {
    form = (
      <div>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={8}>
            <Grid item>
              <TextField
                id="name"
                name="name"
                label="Name"
                value={formik.values.name}
                onChange={formik.handleChange}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />
            </Grid>
            <Grid item>
              <TextField
                id="email"
                name="email"
                label="Email"
                value={formik.values.email}
                onChange={formik.handleChange}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Grid>
            <Grid item>
              <TextField
                id="password"
                name="password"
                label="Password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </Grid>
            <Grid item>
              <MultiSelectJL
                label="Roles"
                field_name="roles"
                formik={formik}
                selected_option_list={enums_to_id_list(userDetail.roles)}
                available_option_list={roleList}
              />
            </Grid>
            <Grid item>
              <MultiSelectJL
                label="Organizations"
                field_name="organizations"
                formik={formik}
                selected_option_list={enums_to_id_list(
                  userDetail.organizations
                )}
                available_option_list={organizationList}
              />
            </Grid>
          </Grid>

          <Box m={2}>
            <Button color="primary" variant="contained" type="submit">
              Update
            </Button>
          </Box>
        </form>
      </div>
    );
  } else if (!isLoading && isLoadError) {
    form = <div></div>;
    error_message = <div id="error_message">{loadErrorMessage}</div>;
  }

  return (
    <Container>
      <h1>User details:</h1>
      {form}
      {error_message}
      <Button component={Link} to="/users">
        Back
      </Button>
      <ToastMessage
        isOpen={isShowToast}
        severity={toastSeverity}
        message={toastMessage}
      />
    </Container>
  );
});
