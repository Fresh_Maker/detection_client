import React from "react";

import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";

import "./LoginLeyout.css";

const LoginLayout: React.FunctionComponent = (props) => {
	// styles
	const useStyles = makeStyles((theme) => ({
		paper: {
			marginTop: theme.spacing(15),
			display: "flex",
			flexDirection: "column",
			alignItems: "center",
		},
		avatar: {
			margin: theme.spacing(1),
		},
		form: {
			width: "100%", // Fix IE 11 issue.
			marginTop: theme.spacing(1),
		},
		submit: {
			margin: theme.spacing(3, 0, 2),
		},
	}));
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
			<div className={classes.paper}>
				<div className="login_icon"></div>
        {props.children}
      </div>
    </Container>
  );
};

export default LoginLayout;
