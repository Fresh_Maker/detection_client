import React, { useCallback } from "react";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import Drawer from "@material-ui/core/Drawer";
import { makeStyles } from "@material-ui/core/styles";

import { DRAWER_WIDTH } from "../../helper/config";
import { I_ui_page } from "../../interfaces/common_interface";

interface ISidebar {
  pages: I_ui_page[];
}

// styles
const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  drawer: {
    width: DRAWER_WIDTH,
  },
  activeNavLink: {
    backgroundColor: theme.palette.action.selected,
  },
}));

const Sidebar: React.FunctionComponent<ISidebar> = (props: ISidebar): JSX.Element => {
  const classes = useStyles();

  return (
    <Drawer
      variant="permanent"
      anchor="left"
      classes={{
        paper: classes.drawer,
      }}
    >
      <div>
        <Box
          className={classes.toolbar}
          px={2}
          display="flex"
          alignItems="center"
        >
          <Typography variant="h5">NeatLeaf</Typography>
        </Box>
        <Divider />
        <List>
          {props.pages.map(({ title, path, icon }, index) => (
            <ListItem
              button
              key={index}
              component={NavLink}
              to={path}
              activeClassName={classes.activeNavLink}
            >
              <ListItemIcon>{icon}</ListItemIcon>
              <ListItemText>{title}</ListItemText>
            </ListItem>
          ))}
        </List>
      </div>
    </Drawer>
  );
};

export default Sidebar;