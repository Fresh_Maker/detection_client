import React, { useCallback } from "react";
import { Route, Switch, useHistory } from "react-router-dom";

import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, withStyles } from "@material-ui/core/styles";

import { DRAWER_WIDTH } from "../../helper/config";
import { I_ui_page } from "../../interfaces/common_interface";

const CustomButton = withStyles({
  root: {
    textTransform: "none",
  },
})(Button);

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
  },
  contentArea: {
    width: `calc(100% - ${DRAWER_WIDTH})`,
    marginLeft: DRAWER_WIDTH,
  },
}));

// interface IContentArea {
//   pages: I_ui_page[];
// }

// const ContentArea: React.FunctionComponent<IContentArea> = (props: IContentArea): JSX.Element => {
const ContentArea = (props) => {
  const classes = useStyles();
  const history = useHistory();

  const handleLogout = useCallback(() => {
    localStorage.removeItem("AuthContext");
    history.push("");
  }, []);

  return (
    <div className={classes.contentArea}>
      <Switch>
        {props.pages.map(({ path, component: Component, icon, title }, index) => (
          <Route path={path} key={index}>
            <AppBar position="static" >
              <Toolbar>
                <Box mr={1}>{icon}</Box>
                <Typography variant="h6" className={classes.title}>
                  {title}
                </Typography>
                <CustomButton color="inherit" onClick={handleLogout}>
                  Log out
                </CustomButton>
              </Toolbar>
            </AppBar>
            <br />
            <br />
            <Container maxWidth="xl">
              <Component />
            </Container>
          </Route>
        ))}
      </Switch>
    </div>
  );
};

export default ContentArea;