import { Box, Button, Container, Grid } from "@material-ui/core";
import { Color } from "@material-ui/lab/Alert";
import ConfirmDialogue from "components/ConfirmDialogue";
import ToastMessage from "components/ToastMessage";
import React, {
  SyntheticEvent,
  useCallback, useMemo,
  useState
} from "react";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import styled from "styled-components";

interface I_with_id {
  id: number
}

export interface IDataTableJLProps<T extends I_with_id> {
  columns: any;
  filter_fn: (filterText: string) => (value: T, index: number, array: T[]) => unknown;
  delete_fn: (id: number) => Promise<any>;
  create_url: string;
  title: string,
  data: T[]
}

export default function DataTableJL<T extends I_with_id>(props: IDataTableJLProps<T>) {
  const { data, title, filter_fn, create_url, columns, delete_fn } = props;

  const [itemList, setItemList] = useState(data as T[]);
  const [selectedItemList, setSelectedItemList] = useState([] as T[]);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);
  const [isShowDeleteButton, setIsShowDeleteButton] = useState(false);
  const [resetPaginationToggle, setResetPaginationToggle] = useState(false);

  const [toastSeverity, setToastSeverity] = useState("success" as Color);
  const [toastMessage, setToastMessage] = useState("success");
  const [isShowToast, setIsShowToast] = useState(false);

  const [isClearSelectedRows, setIsClearSelectedRows] = useState(false);

  const [filterText, setFilterText] = useState("");

  const filteredItems = itemList.filter(filter_fn(filterText));

  const handleCloseDeleteModal = useCallback(
    (e: SyntheticEvent<HTMLButtonElement, Event>) => {
      setIsShowDeleteModal(!isShowDeleteModal);
    },
    [isShowDeleteModal]
  );
  const handleFilter = (e: SyntheticEvent<HTMLInputElement, Event>) => {
    setFilterText(e.currentTarget.value);
    setTimeout(() => document.getElementById(`search`)?.focus());
  };
  const FilterComponent = useCallback(
    ({
      filterText,
      onFilter,
      onClear,
    }: {
      filterText: string;
      onFilter: (e: SyntheticEvent<HTMLInputElement, Event>) => void;
      onClear: () => void;
    }) => {
      const TextField = styled.input`
        height: 32px;
        width: 200px;
        border-radius: 3px;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border: 1px solid #e5e5e5;
        padding: 0 32px 0 16px;

        &:hover {
          cursor: pointer;
        }
      `;

      const ClearButton = styled(Button)`
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        height: 34px;
        width: 32px;
        text-align: center;
        display: inline;
        align-items: center;
        justify-content: center;
      `;

      return (
        <>
          <Grid container justify="flex-end" spacing={1}>
            <Grid item>
              <Box display={isShowDeleteButton ? "inline" : "none"}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={handleCloseDeleteModal}
                >
                  delete
                </Button>
              </Box>
            </Grid>
            <Grid item>
              <Button
                component={Link}
                to={create_url}
                variant="contained"
                color="primary"
              >
                create
              </Button>
            </Grid>
            <Grid item>
              <TextField
                id="search"
                type="text"
                placeholder="Seach"
                aria-label="Search Input"
                value={filterText}
                onChange={onFilter}
              />
              <ClearButton type="button" onClick={onClear}>
                X
              </ClearButton>
            </Grid>
          </Grid>
        </>
      );
    },
    [create_url, handleCloseDeleteModal, isShowDeleteButton]
  );

  const subHeaderComponentMemo = useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText("");
      }
    };

    return (
      <FilterComponent
        onFilter={handleFilter}
        onClear={handleClear}
        filterText={filterText}
      />
    );
  }, [FilterComponent, filterText, resetPaginationToggle]);

  const handleDelete = () => {
    const delete_promise_list = selectedItemList.map((item) => {
      return delete_fn(item.id);
    });
    Promise.all(delete_promise_list).then((response_list) => {
      const error_delete_promise_list = response_list.filter(
        (response) => response.error !== undefined
      );

      if (error_delete_promise_list.length > 0) {
        setToastSeverity("error");
        const message = error_delete_promise_list.reduce((acc, response) => {
          acc += response.error.message + "\n";
          return acc;
        }, "");
        setToastMessage("Delete: " + message);
      } else {
        setToastSeverity("success");
        setToastMessage("Delete: success");
        setIsShowDeleteModal(false);
        setIsShowDeleteButton(false);
        setIsClearSelectedRows(true);
        const deleted_id_set = new Set(
          response_list.map((response) => {
            return response.item?.id;
          })
        );
        setItemList(
          itemList.filter((enuum) => {
            return !deleted_id_set.has(enuum.id);
          })
        );
      }
      setIsShowToast(true);
    });
  };

  const handleSelectedRowsChange = (state: {
    allSelected: boolean;
    selectedCount: number;
    selectedRows: T[];
  }) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    console.log("Selected Rows: ", state.selectedRows);
    setSelectedItemList(state.selectedRows);
    if (state.selectedCount > 0) {
      setIsShowDeleteButton(true);
    } else {
      setIsShowDeleteButton(false);
    }
  };

  const dataTable = (
      <div>
        <DataTable
          title={title}
          selectableRows
          pagination
          columns={columns}
          data={filteredItems}
          subHeader
          subHeaderComponent={subHeaderComponentMemo}
          persistTableHead
          onSelectedRowsChange={handleSelectedRowsChange}
          clearSelectedRows={isClearSelectedRows}
        />
        <ConfirmDialogue
          isOpen={isShowDeleteModal}
          title="Delete?"
          handleCloseDialogue={handleCloseDeleteModal}
          handleConfirm={handleDelete}
        />
      </div>
    );
  return (
    <Container>
      <ToastMessage
        isOpen={isShowToast}
        message={toastMessage}
        severity={toastSeverity}
      />
      {dataTable}
    </Container>
  );
}
