import { InputLabel, Select, Chip, MenuItem } from "@material-ui/core";
import { FormikValues } from "formik";
import { I_enums } from "interfaces/database_interface";
import * as React from "react";
import { ChangeEvent } from "react";

export interface IAppProps {
  label: string;
  field_name: string;
  formik: FormikValues;
  selected_option_list: number[];
  available_option_list: I_enums[];
}

const id_to_label = (number: number, enum_list: I_enums[]) => {
  const found_index = enum_list.findIndex((enums) => enums.id === number);
  if (found_index !== -1) {
    return enum_list[found_index].value;
  }
  return "";
};

export default function App(props: IAppProps) {
  const {
    formik,
    available_option_list,
    label,
    selected_option_list,
    field_name,
  } = props;

  const get_select_value = (field_name: string) => {
    if (selected_option_list && selected_option_list.length > 0) {
      return formik.getFieldProps(field_name).value;
    } else {
      return [];
    }
  };

  const handle_select_on_change = (field_name: string) => {
    const handle_select_on_change_helper = (
      event: ChangeEvent<{ name?: string | undefined; value: unknown }>
    ) => {
      formik.setFieldValue(field_name, event.target.value);
    };
    return handle_select_on_change_helper;
  };

  return (
    <div>
      <InputLabel shrink id={field_name + "-list-label"}>
        {label}
      </InputLabel>

      <Select
        id={field_name + "_select"}
        name={field_name}
        label="Type"
        labelId={field_name + "-list-label"}
        multiple
        value={get_select_value(field_name)}
        onChange={handle_select_on_change(field_name)}
        error={formik.touched.roles && Boolean(formik.errors.roles)}
        renderValue={(selected) => (
          <div>
            {(selected as number[]).map((value) => (
              <Chip
                key={value}
                label={id_to_label(value, available_option_list)}
              />
            ))}
          </div>
        )}
      >
        {available_option_list.map((enumType: I_enums) => (
          <MenuItem key={enumType.id} value={enumType.id}>
            {enumType.value}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
}
