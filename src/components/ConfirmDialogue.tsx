import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import { SyntheticEvent } from 'react';

export interface IConfirmDialogueProps {
  isOpen: boolean,
  handleCloseDialogue: (e: SyntheticEvent<HTMLButtonElement, Event>) => void,
  handleConfirm: (e: SyntheticEvent<HTMLButtonElement, Event>) => void,
  title: string
}

export default function ConfirmDialogue (props: IConfirmDialogueProps) {
  
  return (
    <Dialog
      maxWidth={"sm"}
      open={props.isOpen}
      onClose={props.handleCloseDialogue}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleCloseDialogue} color="primary">
          Cancel
        </Button>
        <Button onClick={props.handleConfirm} color="primary" autoFocus>
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
