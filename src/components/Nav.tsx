import React, { useState } from "react";
import { Link } from "react-router-dom";

import {
  BottomNavigation,
  BottomNavigationAction,
  makeStyles,
} from "@material-ui/core";

import { I_Base } from "../interfaces/common_interface";

export interface INavProps extends I_Base {}

const useStyles = makeStyles({
  root: {
    width: 500,
  },
});

export default function Nav(props: INavProps) {
  const [value, setValue] = useState(0);
  const classes = useStyles();
  const handleOnChange = (event: React.ChangeEvent<{}>, newValue: any) => {
    setValue(newValue);
  };

  return (
    <BottomNavigation
      value={value}
      onChange={handleOnChange}
      showLabels
      className={classes.root}
    >
      <BottomNavigationAction component={Link} to="/user/home" label="Home"/>
      
      <BottomNavigationAction component={Link} to="/user/about" label="About"/>
      
      <BottomNavigationAction component={Link} to="/user/users" label="Users"/>
      <BottomNavigationAction component={Link} to="/user/enums" label="Enums"/>
      
    </BottomNavigation>
  );
}
