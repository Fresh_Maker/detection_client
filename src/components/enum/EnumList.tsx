import { Box, Button, Container, Grid } from "@material-ui/core";
import { Color } from "@material-ui/lab/Alert";
import { delete_enum, get_enum_list } from "api/enums_api";
import ConfirmDialogue from "components/ConfirmDialogue";
import ToastMessage from "components/ToastMessage";
import { I_find_count } from "interfaces/common_interface";
import { I_enums } from "interfaces/database_interface";
import React, {
  SyntheticEvent,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import styled from "styled-components";

export interface IEnumProps {}

export default React.memo((props: IEnumProps) => {
  const [isLoading, setIsLoading] = useState(true);
  const [enumList, setEnumList] = useState([] as I_enums[]);
  const [selectedEnumList, setSelectedEnumList] = useState([] as I_enums[]);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);
  const [isShowDeleteButton, setIsShowDeleteButton] = useState(false);
  const [resetPaginationToggle, setResetPaginationToggle] = useState(false);

  const [toastSeverity, setToastSeverity] = useState("success" as Color);
  const [toastMessage, setToastMessage] = useState("success");
  const [isShowToast, setIsShowToast] = useState(false);

  const [isClearSelectedRows, setIsClearSelectedRows] = useState(false);

  const [filterText, setFilterText] = useState("");

  const filteredItems = enumList.filter(
    (item) =>
      item.code && item.code.toLowerCase().includes(filterText.toLowerCase())
  );

  const handleCloseDeleteModal = useCallback(
    (e: SyntheticEvent<HTMLButtonElement, Event>) => {
      setIsShowDeleteModal(!isShowDeleteModal);
    },
    [isShowDeleteModal]
  );
  const handleFilter = (e: SyntheticEvent<HTMLInputElement, Event>) => {
    setFilterText(e.currentTarget.value);
    setTimeout(() => document.getElementById(`search`)?.focus());
  };
  const FilterComponent = useCallback(
    ({
      filterText,
      onFilter,
      onClear,
    }: {
      filterText: string;
      onFilter: (e: SyntheticEvent<HTMLInputElement, Event>) => void;
      onClear: () => void;
    }) => {
      const TextField = styled.input`
        height: 32px;
        width: 200px;
        border-radius: 3px;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border: 1px solid #e5e5e5;
        padding: 0 32px 0 16px;

        &:hover {
          cursor: pointer;
        }
      `;

      const ClearButton = styled(Button)`
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        height: 34px;
        width: 32px;
        text-align: center;
        display: inline;
        align-items: center;
        justify-content: center;
      `;

      return (
        <>
          <Grid container justify="flex-end" spacing={1}>
            <Grid item>
              <Box display={isShowDeleteButton ? "inline" : "none"}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={handleCloseDeleteModal}
                >
                  delete
                </Button>
              </Box>
            </Grid>
            <Grid item>
              <Button
                component={Link}
                to="enums/create"
                variant="contained"
                color="primary"
              >
                create
              </Button>
            </Grid>
            <Grid item>
              <TextField
                id="search"
                type="text"
                placeholder="Filter By Code"
                aria-label="Search Input"
                value={filterText}
                onChange={onFilter}
              />
              <ClearButton type="button" onClick={onClear}>
                X
              </ClearButton>
            </Grid>
          </Grid>
        </>
      );
    },
    [handleCloseDeleteModal, isShowDeleteButton]
  );

  const subHeaderComponentMemo = useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText("");
      }
    };

    return (
      <FilterComponent
        onFilter={handleFilter}
        onClear={handleClear}
        filterText={filterText}
      />
    );
  }, [FilterComponent, filterText, resetPaginationToggle]);

  const columns = useMemo(
    () => [
      {
        name: "Code",
        selector: "code",
        sortable: true,
        cell: (row: any) => <Link to={`/enums/${row.id}`}>{row.code}</Link>,
      },
      {
        name: "Type",
        selector: "type",
        sortable: true,
      },
      {
        name: "Value",
        selector: "value",
        sortable: true,
      },
    ],
    []
  );

  const handleDelete = () => {
    const delete_promise_list = selectedEnumList.map((enuum) => {
      return delete_enum(enuum.id);
    });
    Promise.all(delete_promise_list).then((response_list) => {
      const error_delete_promise_list = response_list.filter(
        (response) => response.error !== undefined
      );

      if (error_delete_promise_list.length > 0) {
        setToastSeverity("error");
        const message = error_delete_promise_list.reduce((acc, response) => {
          acc += response.error.message + "\n";
          return acc;
        }, "");
        setToastMessage("Delete: " + message);
      } else {
        setToastSeverity("success");
        setToastMessage("Delete: success");
        setIsShowDeleteModal(false);
        setIsShowDeleteButton(false);
        setIsClearSelectedRows(true);
        const deleted_id_set = new Set(
          response_list.map((response) => {
            return response.item?.code;
          })
        );
        setEnumList(
          enumList.filter((enuum) => {
            return !deleted_id_set.has(enuum.code);
          })
        );
      }
      setIsShowToast(true);
    });
  };

  const handleSelectedRowsChange = (state: {
    allSelected: boolean;
    selectedCount: number;
    selectedRows: I_enums[];
  }) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    console.log("Selected Rows: ", state.selectedRows);
    setSelectedEnumList(state.selectedRows);
    if (state.selectedCount > 0) {
      setIsShowDeleteButton(true);
    } else {
      setIsShowDeleteButton(false);
    }
  };

  useEffect(() => {
    setIsLoading(true);
    get_enum_list().then((response: I_find_count<I_enums>) => {
      setIsLoading(false);
      if (response && response.list) {
        setEnumList(response.list);
      }
    });
  }, []);

  let dataTable = <div>Loading enum list...</div>;
  if (!isLoading) {
    dataTable = (
      <div>
        <DataTable
          title="Enum List"
          selectableRows
          pagination
          columns={columns}
          data={filteredItems}
          subHeader
          subHeaderComponent={subHeaderComponentMemo}
          persistTableHead
          onSelectedRowsChange={handleSelectedRowsChange}
          clearSelectedRows={isClearSelectedRows}
        />
        <ConfirmDialogue
          isOpen={isShowDeleteModal}
          title="Delete?"
          handleCloseDialogue={handleCloseDeleteModal}
          handleConfirm={handleDelete}
        />
      </div>
    );
  }
  return (
    <Container>
      <ToastMessage
        isOpen={isShowToast}
        message={toastMessage}
        severity={toastSeverity}
      />
      {dataTable}
    </Container>
  );
});
