import {
  Box,
  Button,
  Container,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from "@material-ui/core";
import { Color } from "@material-ui/lab/Alert";
import {
  create_enum,
  get_enum_detail,
  get_enum_type_list,
  update_enum
} from "api/enums_api";
import ToastMessage from "components/ToastMessage";
import { useFormik } from "formik";
import { I_response } from "interfaces/common_interface";
import { I_enums } from "interfaces/database_interface";
import * as React from "react";
import { useEffect, useState } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import * as yup from "yup";

interface MatchParams {
  id: string;
}

export interface IEnumDetailProps extends RouteComponentProps<MatchParams> {}

export default React.memo((props: IEnumDetailProps) => {
  const enum_id = props.match.params.id;
  const [isLoading, setIsLoading] = useState(true);
  const [isShowToast, setIsShowToast] = useState(false);
  const [toastSeverity, setToastSeverity] = useState("success" as Color);
  const [toastMessage, setToastMessage] = useState("success");
  const [enumDetail, setEnumDetail] = useState({
    code: "BAR",
    type: "",
    value: "foo",
    id: -1,
    metadata: {},
  } as I_enums);
  const [enumTypeList, setEnumTypeList] = useState([] as string[]);

  useEffect(() => {
    setIsLoading(true);
    console.log(`enum_id: ${enum_id}`);
    if (enum_id !== undefined) {
      get_enum_detail(enum_id).then((response: I_enums) => {
        setIsLoading(false);
        setEnumDetail(response);
      });
    }

    get_enum_type_list().then((response: string[]) => {
      setIsLoading(false);
      setEnumTypeList(response);
    });
  }, [enum_id]);

  

  const validationSchema = yup.object({
    code: yup
      .string()
      .strict()
      .trim()
      .uppercase()
      .min(3, "Code should of minimum 3 characters length")
      .max(32, "Code should of maximum 32 characters length")
      .required("Code is required"),
    type: yup
      .string()
      .trim()
      .uppercase()
      .min(3, "Type should of minimum 3 characters length")
      .max(32, "Type should of maximum 32 characters length")
      .required("Type is required"),
    value: yup.string().trim().uppercase().required("Value is required"),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: { ...enumDetail },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      let action_promise = null;
      let action_message = "";
      if (values.id === -1) {
        action_promise = create_enum(values);
        action_message = "Create";
      } else {
        action_promise = update_enum(values);
        action_message = "Update";
      }
      action_promise.then((response: I_response<I_enums>) => {
        if (response.error) {
          setToastSeverity("error");
          setToastMessage(action_message + ": " + response.error.message);
        } else {
          setToastSeverity("success");
          setToastMessage(action_message + ": success");
        }
        setIsShowToast(true);
      });
    },
  });
  
  let form = <div>loading enum form...</div>;
  if (!isLoading){
    form = (
      <div>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={8}>
            <Grid item>
              <TextField
                id="value"
                name="value"
                label="Value"
                value={formik.values.value}
                onChange={formik.handleChange}
                error={formik.touched.value && Boolean(formik.errors.value)}
                helperText={formik.touched.value && formik.errors.value}
              />
            </Grid>
            <Grid item>
              <TextField
                id="code"
                name="code"
                label="Code"
                value={formik.values.code}
                onChange={formik.handleChange}
                error={formik.touched.code && Boolean(formik.errors.code)}
                helperText={formik.touched.code && formik.errors.code}
              />
            </Grid>
            <Grid item>
              <InputLabel shrink id="demo-simple-select-placeholder-label-label">
                Type
              </InputLabel>
              <Select
                id="type_select"
                name="type"
                label="Type"
                labelId="demo-simple-select-placeholder-label-label"
                value={formik.values.type}
                onChange={formik.handleChange}
                error={formik.touched.value && Boolean(formik.errors.value)}
              >
                {enumTypeList.map((enumType: string) => (
                  <MenuItem key={enumType} value={enumType}>
                    {enumType}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
          </Grid>
  
          <Box m={2}>
            <Button color="primary" variant="contained" type="submit">
              Update
            </Button>
          </Box>
        </form>
      </div>
    );
  }

  return (
    <Container>
      <ToastMessage
        isOpen={isShowToast}
        severity={toastSeverity}
        message={toastMessage}
      />
      <h1>Enum details:</h1>
      {form}
      <Box m={3}>
        <Button component={Link} to="/enums">
          Back
        </Button>
      </Box>
    </Container>
  );
});
