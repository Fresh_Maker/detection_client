import { Snackbar } from "@material-ui/core";
import * as React from "react";
import MuiAlert, { Color } from "@material-ui/lab/Alert";
export interface IToastMessageProps {
  isOpen: boolean,
  severity: Color,
  message: string
}

export default function ToastMessage(props: IToastMessageProps) {
  
  return (
    <Snackbar
      open={props.isOpen}
      autoHideDuration={6000}
    >
      <MuiAlert  severity={props.severity}>
        {props.message}
      </MuiAlert>
    </Snackbar>
  );
}
