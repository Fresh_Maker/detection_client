// import { Component, ComponentType } from "react";

/**
 * Base interface from which will be derived other interfaces so it will be easy to cast.
 */
export interface I_Base {}

export interface I_find_count<Type> {
  total?: number;
  list?: Type[];
  error?: any;
}

export interface I_response<Type> {
  error?: any;
  item?: Type;
}


/**
 * UI page interfaces.
 */
export interface I_ui_page {
  title: string,
  path: string,
  icon?: JSX.Element,
  component: Function | Element | JSX.Element // | React.LazyExoticComponent<React.MemoExoticComponent<(props: I_Base) => JSX.Element>>,
}
