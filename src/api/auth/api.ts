import { useHistory } from "react-router-dom";

import axios from "axios";
export const BASE_URL = "http://localhost:3000/";
//const SERVER_URL = process.env.REACT_APP_SERVER_URL;

const ApiClient = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

// Auth request configuration option change or doing validations.
ApiClient.interceptors.request.use(
  async function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// Global error handling.
ApiClient.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default ApiClient;
