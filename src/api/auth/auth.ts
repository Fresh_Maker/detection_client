import ApiClient from "./api";

const AuthClient = {
  login: function (email: string, password: string) {
    var data = {
      email,
      password
    }

    return ApiClient.post("/login", data);
  },
};

export default AuthClient;
