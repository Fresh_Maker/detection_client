import axios from "axios";
import { I_find_count, I_response } from "interfaces/common_interface";
import { I_enums } from "interfaces/database_interface";

export const ENUM_BASE_URL = "http://localhost:3000/enums";

export const get_enum_list = (): Promise<I_find_count<I_enums>> => {
  const url = `${ENUM_BASE_URL}/`;
  return axios
    .get(url)
    .then(({ data }: { data: I_find_count<I_enums> }) => data);
};

export const get_enum_detail = (enum_id: string): Promise<I_enums> => {
  const url = `${ENUM_BASE_URL}/${enum_id}`;
  return axios.get(url).then(({ data }) => data);
};

export const get_enum_type_list = (): Promise<string[]> => {
  const url = `${ENUM_BASE_URL}/type_list`;
  return axios.get(url).then(({ data }) => data);
};

export const get_enum_by_type_ = (type: string): Promise<I_enums[]> => {
  const url = `${ENUM_BASE_URL}/type/${type}`;
  return axios.get(url).then(({ data }) => data);
};

export const update_enum = (
  enum_detail: I_enums
): Promise<I_response<I_enums>> => {
  const url = `${ENUM_BASE_URL}/${enum_detail.id}`;
  console.log(`enum_detail: ${JSON.stringify(enum_detail, null, 2)}`);
  return axios.put(url, { ...enum_detail }).then(({ data }) => data);
};

export const create_enum = (
  enum_detail: I_enums
): Promise<I_response<I_enums>> => {
  const url = `${ENUM_BASE_URL}`;
  console.log(`enum_detail: ${JSON.stringify(enum_detail, null, 2)}`);
  return axios.post(url, { ...enum_detail }).then(({ data }) => data);
};

export const delete_enum = (enum_id: number): Promise<I_response<I_enums>> => {
  const url = `${ENUM_BASE_URL}`;
  return axios.delete(url + "/" + enum_id).then(({ data }) => data);
};
