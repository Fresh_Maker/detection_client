import axios from "axios";
import { I_find_count, I_response } from "../interfaces/common_interface";
import { I_user } from "../interfaces/database_interface";

export const USER_BASE_URL = "http://localhost:3000/users";
// export const USER_BASE_URL = "http://localhost:3000/graphql";

export const get_user_list = (): Promise<I_find_count<I_user>> => {
  const url = `${USER_BASE_URL}/`;
  return axios
    .get(url)
    .then(({ data }: { data: I_find_count<I_user> }) => data);
};

export const get_user_detail = (user_id: string): Promise<I_response<I_user>> => {
  const url = `${USER_BASE_URL}/${user_id}`;
  return axios.get(url).then(({ data }) => data);
};

export const update_user = (
  user_detail: I_user
): Promise<I_response<I_user>> => {
  const url = `${USER_BASE_URL}/${user_detail.id}`;
  console.log(`user_detail: ${JSON.stringify(user_detail, null, 2)}`);
  return axios.put(url, { ...user_detail }).then(({ data }) => data);
};

export const create_user = (
  user_detail: I_user
): Promise<I_response<I_user>> => {
  const url = `${USER_BASE_URL}`;
  console.log(`user_detail: ${JSON.stringify(user_detail, null, 2)}`);
  return axios.post(url, { ...user_detail }).then(({ data }) => data);
};

export const delete_user = (enum_id: number): Promise<I_response<I_user>> => {
  const url = `${USER_BASE_URL}`;
  return axios.delete(url + "/" + enum_id).then(({ data }) => data);
};