import { Chip, Container } from "@material-ui/core";
import { delete_user, get_user_list } from "api/users_api";
import DataTableJL from "components/DataTableJL";
import { I_find_count } from "interfaces/common_interface";
import { I_enums, I_user } from "interfaces/database_interface";
import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";

export interface IUserProps {}

export default React.memo((props: IUserProps) => {
  const [isLoading, setIsLoading] = useState(true);
  const [userList, setUserList] = useState([] as I_user[]);

  const columns = useMemo(
    () => [
      {
        name: "Name",
        selector: "name",
        sortable: true,
        cell: (row: any) => <Link to={`/users/${row.id}`}>{row.name}</Link>,
      },
      {
        name: "Email",
        selector: "email",
        sortable: true,
      },
      {
        name: "Roles",
        selector: "roles",
        cell: ({ roles }: { roles: I_enums[] }) => {
          const role_list = roles
            .sort((a, b) => {
              if (a.value < b.value) {
                return -1;
              }
              if (a.value > b.value) {
                return 1;
              }
              return 0;
            })
            .map((role: I_enums, index: number) => (
              <Chip key={index} label={role.value} />
            ));
          return <div className="role-list">{role_list}</div>;
        },
      },
    ],
    []
  );

  useEffect(() => {
    setIsLoading(true);
    get_user_list().then((response: I_find_count<I_user>) => {
      if (response.list) {
        setUserList(response.list);
      }
      setIsLoading(false);
    });
  }, []);
  const filter_fn = (filterText: string) => {
    const x = (item: I_user) =>
      item.name && item.name.toLowerCase().includes(filterText.toLowerCase());
    return x;
  };
  const delete_fn = (id: number) => {
    return delete_user(id);
  };
  let html = <div>Loading user list...</div>;
  if (!isLoading) {
    html = (
      <div>
        <DataTableJL
          title="User List"
          columns={columns}
          data={userList}
          create_url="users/create"
          filter_fn={filter_fn}
          delete_fn={delete_fn}
        />
      </div>
    );
  }

  return <Container>{html}</Container>;
});
