import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import InfoIcon from "@material-ui/icons/Info";
import GroupIcon from "@material-ui/icons/Group";
import { Container } from "@material-ui/core";
import Nav from "../../components/Nav";

import ContentArea from "../../components/common/ContentArea";
import { I_Base, I_ui_page } from "../../interfaces/common_interface";

import UserList from "../../components/user/UserList";
import EnumList from "../../components/enum/EnumList";
// const UserList = lazy(()=> import("../../components/user/UserList"))
// const EnumList = lazy(()=> import("../../components/enum/EnumList"))

import Sidebar from "../../components/common/Sidebar";

const HomePage = () => {
  return (
    <Container>
      <h2>Home</h2>
    </Container>
  );
}

const AboutPage = () => {
  return (
    <Container>
      <h2>About</h2>
    </Container>
  );
}

const pages: I_ui_page[] = [
  {
    title: "Home",
    path: "/user/home",
    icon: <HomeIcon />,
    component: HomePage,
  },
  {
    title: "About",
    path: "/user/about",
    icon: <InfoIcon />,
    component: AboutPage,
  },
  {
    title: "Users",
    path: "/user/users",
    icon: <GroupIcon />,
    component: UserList,
  },
  {
    title: "Enums",
    path: "/user/Enums",
    component: EnumList,
  },
];

export default function User() {
  return (
    <React.Fragment>
      {/* <Nav></Nav> */}
      <Sidebar pages={pages} />
      <ContentArea
        pages={pages}
      />
    </React.Fragment>
  );
}

// {/* <Suspense fallback={<Container>Loading Suspense...</Container>}> */}
//           {/* A <Switch> looks through its children <Route>s and
//               renders the first one that matches the current URL. */}
//         <ContentArea
//           pages={pages}
//         />
//       {/* </Suspense> */}