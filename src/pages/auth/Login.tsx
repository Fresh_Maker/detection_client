import React from "react";
import { useHistory } from "react-router-dom";

import { Formik } from "formik";
import * as yup from "yup";

import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

import LoginLayout from "../../components/common/LoginLeyout";
import AuthClient from "../../api/auth/auth";
import { makeStyles } from "@material-ui/core/styles";

import FormikInputField from "../../components/formik/FormikInputField";
// import jwtDecode from "jwt-decode";

const LoginSchema = yup.object().shape({
	username: yup
		.string()
		.email("Enter a valid email")
		.required("Email is required"),
	password: yup.string().required("Password is required"),
});

const Login: React.FunctionComponent = () => {
	const useStyles = makeStyles((theme) => ({
		paper: {
			marginTop: theme.spacing(8),
			display: "flex",
			flexDirection: "column",
			alignItems: "center",
		},
		avatar: {
			margin: theme.spacing(1),
			backgroundColor: theme.palette.secondary.main,
		},
		form: {
			width: "100%", // Fix IE 11 issue.
			marginTop: theme.spacing(1),
		},
		submit: {
			margin: theme.spacing(3, 0, 2),
			background: 'linear-gradient(to right, #0b6ca6 0%,#159199 100%)',
			border: 0,
			borderRadius: 3,
			boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
			color: 'white',
			height: 48,
			padding: '0 30px',
		},
	}))
	const classes = useStyles();
	const history = useHistory();

	const setError = (error: any, setErrors: Function) => {
		if (error.response && error.response.status) {
			setErrors({
				message: "Email or password are incorrect",
			});
		} else {
			setErrors({
				message: "Server is unreachable",
			});
		}
	};

	const onSubmit = (values: any, setSubmitting: Function, setErrors: Function) => {
		const data = {
			loggedIn: true,
			access_token: "",
			refresh_token: "",
			expires_in: new Date(),
			email: values.username,
		};
		localStorage.setItem("auth_context", JSON.stringify(data));
		setSubmitting(false);
		history.push('user/home/');

		// AuthClient.login(values.username, values.password)
		// 	.then((result: any) => {
		// 		const expires_in = new Date(Date.now() + result.data.expires_in * 1000);
		// 		const data = {
		// 			loggedIn: true,
		// 			access_token: result.data.access_token,
		// 			refresh_token: result.data.refresh_token,
		// 			expires_in,
		// 			email: values.username,
		// 		};
		// 		localStorage.setItem("auth_context", JSON.stringify(data));
		// 	})
		// 	.catch((error: any) => {
		// 		setError(error, setErrors);
		// 	})
		// 	.finally(() => {
		// 		setSubmitting(false);
		// 	});
	};

	return (
		<LoginLayout>
			<Formik
				initialValues={{ username: "", password: "", message: "" }}
				validationSchema={LoginSchema}
				onSubmit={(values, { setSubmitting, setErrors }) => {
					onSubmit(values, setSubmitting, setErrors);
				}}
			>
				{({
					values,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					isSubmitting,
				}) => (
					<form className={classes.form} onSubmit={handleSubmit}>
						{/* <Grid
							container
							direction="column"
							justify="center"
							alignItems="center"
						>
							<Grid item>
								<Box>{"Don't have an account?"}</Box>
							</Grid>
							<Grid item>
								<Link href="/registration" variant="body2">
									{"Register your Organization here."}
								</Link>
							</Grid>
						</Grid> */}
						<FormikInputField
							margin="normal"
							required
							fullWidth
							id="username"
							label="Email Address"
							name="username"
							value={values.username}
							autoComplete="email"
							autoFocus
							onBlur={handleBlur}
							onChange={handleChange}
						/>
						<FormikInputField
							margin="normal"
							required
							fullWidth
							name="password"
							value={values.password}
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
							onBlur={handleBlur}
							onChange={handleChange}
						/>
						<Box
							display={errors.message ? "" : "none"}
							color={errors.message ? "error.main" : "success.main"}
						>
							{errors.message}
						</Box>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							className={classes.submit}
							disabled={isSubmitting}
						>
							Sign In
						</Button>
						{/* <Grid container justify="center" alignItems="center">
							<Grid item>
								<Link href="/forgotpassword" variant="body2">
									Forgot password?
								</Link>
							</Grid>
						</Grid> */}
					</form>
				)}
			</Formik>
		</LoginLayout>
	);
};

export default Login;
