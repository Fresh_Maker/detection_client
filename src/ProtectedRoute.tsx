import React from "react";
import { Route, Redirect } from "react-router-dom";

interface IProtectedRoute {
  path: any,
  component: any
}

export const ProtectedRoute: React.FunctionComponent<IProtectedRoute> = ({ component: Component, ...rest }) => {
  const is_logged_in = (): boolean => {
    const auth_context = localStorage.getItem("auth_context");
    if (auth_context === null) {
      return false;
    }
    const auth_context_obj = JSON.parse(auth_context);
    return auth_context_obj.loggedIn;
  }

  return (
    <Route
      {...rest}
      render={(props) => {
        if (is_logged_in()) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
};
