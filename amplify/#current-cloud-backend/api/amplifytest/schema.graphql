type Blog @model {
  id: ID!
  name: String!
  posts: [Post] @connection(keyName: "byBlog", fields: ["id"])
}

type Post @model @key(name: "byBlog", fields: ["blogID"]) {
  id: ID!
  title: String!
  blogID: ID!
  blog: Blog @connection(fields: ["blogID"])
  comments: [Comment] @connection(keyName: "byPost", fields: ["id"])
}

type Comment @model @key(name: "byPost", fields: ["postID", "content"]) {
  id: ID!
  postID: ID!
  post: Post @connection(fields: ["postID"])
  content: String!
}

interface Node {
  nodeId: ID!
}

type ClassificationsConnection {
  nodes: [Classification]!

  edges: [ClassificationsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type Classification implements Node {
  nodeId: ID!
  id: Int!
  metadata: String
  nameId: Int

  enumByNameId: Enum

  tracksByClassificationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [TracksOrderBy!] = [PRIMARY_KEY_ASC]

    condition: TrackCondition
  ): TracksConnection!

  userLabelsByClassificationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [UserLabelsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: UserLabelCondition
  ): UserLabelsConnection!

  detectionsByClassificationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DetectionsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DetectionCondition
  ): DetectionsConnection!
}

type Enum implements Node {
  nodeId: ID!
  id: Int!
  value: String!
  type: String!
  code: String!
  metadata: String

  locationsByNameId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [LocationsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: LocationCondition
  ): LocationsConnection!

  deviceByOrganizationId: Device

  devicesByOrganizationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DevicesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceCondition
  ): DevicesConnection!
    @deprecated(reason: "Please use deviceByOrganizationId instead")

  devicesByTypeId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DevicesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceCondition
  ): DevicesConnection!

  labelTasksByMinimumExpertise(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [LabelTasksOrderBy!] = [PRIMARY_KEY_ASC]

    condition: LabelTaskCondition
  ): LabelTasksConnection!

  labelTasksByPriority(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [LabelTasksOrderBy!] = [PRIMARY_KEY_ASC]

    condition: LabelTaskCondition
  ): LabelTasksConnection!

  labelTasksByStatus(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [LabelTasksOrderBy!] = [PRIMARY_KEY_ASC]

    condition: LabelTaskCondition
  ): LabelTasksConnection!

  classificationsByNameId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [ClassificationsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: ClassificationCondition
  ): ClassificationsConnection!

  deviceTypesByTypeId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DeviceTypesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceTypeCondition
  ): DeviceTypesConnection!

  deviceTypesByUomId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DeviceTypesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceTypeCondition
  ): DeviceTypesConnection!

  growthCyclesByStrainId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [GrowthCyclesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: GrowthCycleCondition
  ): GrowthCyclesConnection!

  locationEnmosByNameId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [LocationEnmosOrderBy!] = [PRIMARY_KEY_ASC]

    condition: LocationEnmoCondition
  ): LocationEnmosConnection!

  locationEnmosByOrganizationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [LocationEnmosOrderBy!] = [PRIMARY_KEY_ASC]

    condition: LocationEnmoCondition
  ): LocationEnmosConnection!

  zonesByNameId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [ZonesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: ZoneCondition
  ): ZonesConnection!

  deviceEnmosByStatusId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DeviceEnmosOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceEnmoCondition
  ): DeviceEnmosConnection!

  userOrganizationsByOrganizationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [UserOrganizationsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: UserOrganizationCondition
  ): UserOrganizationsConnection!

  userRolesByRoleId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [UserRolesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: UserRoleCondition
  ): UserRolesConnection!
}

type LocationsConnection {
  nodes: [Location]!

  edges: [LocationsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type Location implements Node {
  nodeId: ID!
  id: Int!
  timezone: Int!
  coordinates: Point
  metadata: String
  nameId: Int

  enumByNameId: Enum

  devicesByLocationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DevicesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceCondition
  ): DevicesConnection!
}

type Point {
  x: Float!
  y: Float!
}

type DevicesConnection {
  nodes: [Device]!

  edges: [DevicesEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type Device implements Node {
  nodeId: ID!
  id: Int!
  organizationId: Int!
  uid: String!
  locationId: Int!
  metadata: String
  typeId: Int

  enumByOrganizationId: Enum

  locationByLocationId: Location

  enumByTypeId: Enum

  tracksByDeviceId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [TracksOrderBy!] = [PRIMARY_KEY_ASC]

    condition: TrackCondition
  ): TracksConnection!

  imageByDeviceId: Image

  imagesByDeviceId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [ImagesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: ImageCondition
  ): ImagesConnection! @deprecated(reason: "Please use imageByDeviceId instead")
}

type TracksConnection {
  nodes: [Track]!

  edges: [TracksEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type Track implements Node {
  nodeId: ID!
  id: Int!
  firstImageTimestamp: Int!
  lastImageTimestamp: Int!
  metadata: String
  classificationId: Int
  deviceId: Int

  classificationByClassificationId: Classification

  deviceByDeviceId: Device

  trackDetectionsByTrackId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [TrackDetectionsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: TrackDetectionCondition
  ): TrackDetectionsConnection!
}

type TrackDetectionsConnection {
  nodes: [TrackDetection]!

  edges: [TrackDetectionsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type TrackDetection implements Node {
  nodeId: ID!
  id: Int!
  trackId: Int!
  detectionId: Int!

  trackByTrackId: Track

  detectionByDetectionId: Detection
}

type Detection implements Node {
  nodeId: ID!
  id: Int!
  detectionRunId: Int!
  roi: String!
  detectionConfidence: Float
  classificationId: Int!
  classificationConfidence: Float
  classificationDistribution: String
  cropResourcePath: String!
  metadata: String

  detectionRunByDetectionRunId: DetectionRun

  classificationByClassificationId: Classification

  trackDetectionsByDetectionId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [TrackDetectionsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: TrackDetectionCondition
  ): TrackDetectionsConnection!

  labelTasksByDetectionId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [LabelTasksOrderBy!] = [PRIMARY_KEY_ASC]

    condition: LabelTaskCondition
  ): LabelTasksConnection!

  userLabelsByDetectionId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [UserLabelsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: UserLabelCondition
  ): UserLabelsConnection!
}

type DetectionRun implements Node {
  nodeId: ID!
  id: Int!
  createTimestamp: Int!
  state: Int!
  startTimestamp: Int
  endTimestamp: Int
  metadata: String
  detectorId: Int
  imageId: Int

  detectorByDetectorId: Detector

  imageByImageId: Image

  detectionsByDetectionRunId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DetectionsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DetectionCondition
  ): DetectionsConnection!
}

type Detector implements Node {
  nodeId: ID!
  id: Int!
  name: String!
  version: String!
  metadata: String

  detectionRunsByDetectorId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DetectionRunsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DetectionRunCondition
  ): DetectionRunsConnection!
}

type DetectionRunsConnection {
  nodes: [DetectionRun]!

  edges: [DetectionRunsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type DetectionRunsEdge {
  cursor: String

  node: DetectionRun
}

scalar String

type PageInfo {
  hasNextPage: Boolean!

  hasPreviousPage: Boolean!

  startString: String

  endString: String
}

enum DetectionRunsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  CREATE_TIMESTAMP_ASC
  CREATE_TIMESTAMP_DESC
  STATE_ASC
  STATE_DESC
  START_TIMESTAMP_ASC
  START_TIMESTAMP_DESC
  END_TIMESTAMP_ASC
  END_TIMESTAMP_DESC
  METADATA_ASC
  METADATA_DESC
  DETECTOR_ID_ASC
  DETECTOR_ID_DESC
  IMAGE_ID_ASC
  IMAGE_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input DetectionRunCondition {
  id: Int

  createTimestamp: Int

  state: Int

  startTimestamp: Int

  endTimestamp: Int

  metadata: String

  detectorId: Int

  imageId: Int
}

type Image implements Node {
  nodeId: ID!
  id: Int!
  deviceId: Int!
  timestamp: Int!
  timezone: Int!
  resourcePath: String!
  metadata: String

  deviceByDeviceId: Device

  detectionRunsByImageId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DetectionRunsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DetectionRunCondition
  ): DetectionRunsConnection!
}

type DetectionsConnection {
  nodes: [Detection]!

  edges: [DetectionsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type DetectionsEdge {
  cursor: String

  node: Detection
}

enum DetectionsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  DETECTION_RUN_ID_ASC
  DETECTION_RUN_ID_DESC
  ROI_ASC
  ROI_DESC
  DETECTION_CONFIDENCE_ASC
  DETECTION_CONFIDENCE_DESC
  CLASSIFICATION_ID_ASC
  CLASSIFICATION_ID_DESC
  CLASSIFICATION_CONFIDENCE_ASC
  CLASSIFICATION_CONFIDENCE_DESC
  CLASSIFICATION_DISTRIBUTION_ASC
  CLASSIFICATION_DISTRIBUTION_DESC
  CROP_RESOURCE_PATH_ASC
  CROP_RESOURCE_PATH_DESC
  METADATA_ASC
  METADATA_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input DetectionCondition {
  id: Int

  detectionRunId: Int

  roi: String

  detectionConfidence: Float

  classificationId: Int

  classificationConfidence: Float

  classificationDistribution: String

  cropResourcePath: String

  metadata: String
}

enum TrackDetectionsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  TRACK_ID_ASC
  TRACK_ID_DESC
  DETECTION_ID_ASC
  DETECTION_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input TrackDetectionCondition {
  id: Int

  trackId: Int

  detectionId: Int
}

type LabelTasksConnection {
  nodes: [LabelTask]!

  edges: [LabelTasksEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type LabelTask implements Node {
  nodeId: ID!
  id: Int!
  createTimestamp: Int!
  modifiedTimestamp: Int
  metadata: String
  detectionId: Int
  minimumExpertise: Int
  priority: Int
  status: Int

  detectionByDetectionId: Detection

  enumByMinimumExpertise: Enum

  enumByPriority: Enum

  enumByStatus: Enum
}

type LabelTasksEdge {
  cursor: String

  node: LabelTask
}

enum LabelTasksOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  CREATE_TIMESTAMP_ASC
  CREATE_TIMESTAMP_DESC
  MODIFIED_TIMESTAMP_ASC
  MODIFIED_TIMESTAMP_DESC
  METADATA_ASC
  METADATA_DESC
  DETECTION_ID_ASC
  DETECTION_ID_DESC
  MINIMUM_EXPERTISE_ASC
  MINIMUM_EXPERTISE_DESC
  PRIORITY_ASC
  PRIORITY_DESC
  STATUS_ASC
  STATUS_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input LabelTaskCondition {
  id: Int

  createTimestamp: Int

  modifiedTimestamp: Int

  metadata: String

  detectionId: Int

  minimumExpertise: Int

  priority: Int

  status: Int
}

type UserLabelsConnection {
  nodes: [UserLabel]!

  edges: [UserLabelsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type UserLabel implements Node {
  nodeId: ID!
  id: Int!
  confidence: Int!
  createTimestamp: Int!
  modifiedTimestamp: Int
  metadata: String
  classificationId: Int
  detectionId: Int
  userId: Int

  classificationByClassificationId: Classification

  detectionByDetectionId: Detection

  userByUserId: User
}

type User  implements Node @model{
  nodeId: ID!
  id: Int!
  name: String!
  email: String!
  password: String!
  metadata: String

  userLabelsByUserId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [UserLabelsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: UserLabelCondition
  ): UserLabelsConnection!

  userOrganizationsByUserId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [UserOrganizationsOrderBy!] = [PRIMARY_KEY_ASC]

    condition: UserOrganizationCondition
  ): UserOrganizationsConnection!

  userRolesByUserId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [UserRolesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: UserRoleCondition
  ): UserRolesConnection!
}

enum UserLabelsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  CONFIDENCE_ASC
  CONFIDENCE_DESC
  CREATE_TIMESTAMP_ASC
  CREATE_TIMESTAMP_DESC
  MODIFIED_TIMESTAMP_ASC
  MODIFIED_TIMESTAMP_DESC
  METADATA_ASC
  METADATA_DESC
  CLASSIFICATION_ID_ASC
  CLASSIFICATION_ID_DESC
  DETECTION_ID_ASC
  DETECTION_ID_DESC
  USER_ID_ASC
  USER_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input UserLabelCondition {
  id: Int

  confidence: Int

  createTimestamp: Int

  modifiedTimestamp: Int

  metadata: String

  classificationId: Int

  detectionId: Int

  userId: Int
}

type UserOrganizationsConnection {
  nodes: [UserOrganization]!

  edges: [UserOrganizationsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type UserOrganization implements Node {
  nodeId: ID!
  userId: Int!
  organizationId: Int!

  userByUserId: User

  enumByOrganizationId: Enum
}

type UserOrganizationsEdge {
  cursor: String

  node: UserOrganization
}

enum UserOrganizationsOrderBy {
  NATURAL
  USER_ID_ASC
  USER_ID_DESC
  ORGANIZATION_ID_ASC
  ORGANIZATION_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input UserOrganizationCondition {
  userId: Int

  organizationId: Int
}

type UserRolesConnection {
  nodes: [UserRole]!

  edges: [UserRolesEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type UserRole implements Node {
  nodeId: ID!
  userId: Int!
  roleId: Int!

  userByUserId: User

  enumByRoleId: Enum
}

type UserRolesEdge {
  cursor: String

  node: UserRole
}

enum UserRolesOrderBy {
  NATURAL
  USER_ID_ASC
  USER_ID_DESC
  ROLE_ID_ASC
  ROLE_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input UserRoleCondition {
  userId: Int

  roleId: Int
}

type UserLabelsEdge {
  cursor: String

  node: UserLabel
}

type TrackDetectionsEdge {
  cursor: String

  node: TrackDetection
}

type TracksEdge {
  cursor: String

  node: Track
}

enum TracksOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  FIRST_IMAGE_TIMESTAMP_ASC
  FIRST_IMAGE_TIMESTAMP_DESC
  LAST_IMAGE_TIMESTAMP_ASC
  LAST_IMAGE_TIMESTAMP_DESC
  METADATA_ASC
  METADATA_DESC
  CLASSIFICATION_ID_ASC
  CLASSIFICATION_ID_DESC
  DEVICE_ID_ASC
  DEVICE_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input TrackCondition {
  id: Int

  firstImageTimestamp: Int

  lastImageTimestamp: Int

  metadata: String

  classificationId: Int

  deviceId: Int
}

type ImagesConnection {
  nodes: [Image]!

  edges: [ImagesEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type ImagesEdge {
  cursor: String

  node: Image
}

enum ImagesOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  DEVICE_ID_ASC
  DEVICE_ID_DESC
  TIMESTAMP_ASC
  TIMESTAMP_DESC
  TIMEZONE_ASC
  TIMEZONE_DESC
  RESOURCE_PATH_ASC
  RESOURCE_PATH_DESC
  METADATA_ASC
  METADATA_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input ImageCondition {
  id: Int

  deviceId: Int

  timestamp: Int

  timezone: Int

  resourcePath: String

  metadata: String
}

type DevicesEdge {
  cursor: String

  node: Device
}

enum DevicesOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  ORGANIZATION_ID_ASC
  ORGANIZATION_ID_DESC
  UID_ASC
  UID_DESC
  LOCATION_ID_ASC
  LOCATION_ID_DESC
  METADATA_ASC
  METADATA_DESC
  TYPE_ID_ASC
  TYPE_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input DeviceCondition {
  id: Int

  organizationId: Int

  uid: String

  locationId: Int

  metadata: String

  typeId: Int
}

type LocationsEdge {
  cursor: String

  node: Location
}

enum LocationsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  TIMEZONE_ASC
  TIMEZONE_DESC
  COORDINATES_ASC
  COORDINATES_DESC
  METADATA_ASC
  METADATA_DESC
  NAME_ID_ASC
  NAME_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input LocationCondition {
  id: Int

  timezone: Int

  coordinates: PointInput

  metadata: String

  nameId: Int
}

input PointInput {
  x: Float!
  y: Float!
}

enum ClassificationsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  METADATA_ASC
  METADATA_DESC
  NAME_ID_ASC
  NAME_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input ClassificationCondition {
  id: Int

  metadata: String

  nameId: Int
}

type DeviceTypesConnection {
  nodes: [DeviceType]!

  edges: [DeviceTypesEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type DeviceType implements Node {
  nodeId: ID!
  id: Int!
  description: String
  statusMessage: String
  deviceConfig: String
  defaultAddress: String
  addressOptions: String
  deviceEnmoId: Int
  typeId: Int
  uomId: Int
  monitoringParamsId: Int

  deviceEnmoByDeviceEnmoId: DeviceEnmo

  enumByTypeId: Enum

  enumByUomId: Enum

  monitoringParamByMonitoringParamsId: MonitoringParam
}

type DeviceEnmo implements Node {
  nodeId: ID!
  id: Int!
  description: String
  statusMessage: String
  deviceConfig: String
  zoneId: Int
  statusId: Int

  zoneByZoneId: Zone

  enumByStatusId: Enum

  deviceTypesByDeviceEnmoId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DeviceTypesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceTypeCondition
  ): DeviceTypesConnection!
}

type Zone implements Node {
  nodeId: ID!
  id: Int!
  description: String
  metadata: String
  nameId: Int
  locationId: Int

  enumByNameId: Enum

  locationEnmoByLocationId: LocationEnmo

  growthCyclesByZoneId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [GrowthCyclesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: GrowthCycleCondition
  ): GrowthCyclesConnection!

  deviceEnmosByZoneId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DeviceEnmosOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceEnmoCondition
  ): DeviceEnmosConnection!
}

type LocationEnmo implements Node {
  nodeId: ID!
  id: Int!
  description: String
  metadata: String
  nameId: Int
  organizationId: Int

  enumByNameId: Enum

  enumByOrganizationId: Enum

  zonesByLocationId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [ZonesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: ZoneCondition
  ): ZonesConnection!
}

type ZonesConnection {
  nodes: [Zone]!

  edges: [ZonesEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type ZonesEdge {
  cursor: String

  node: Zone
}

enum ZonesOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  DESCRIPTION_ASC
  DESCRIPTION_DESC
  METADATA_ASC
  METADATA_DESC
  NAME_ID_ASC
  NAME_ID_DESC
  LOCATION_ID_ASC
  LOCATION_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input ZoneCondition {
  id: Int

  description: String

  metadata: String

  nameId: Int

  locationId: Int
}

type GrowthCyclesConnection {
  nodes: [GrowthCycle]!

  edges: [GrowthCyclesEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type GrowthCycle implements Node {
  nodeId: ID!
  id: Int!
  description: String
  title: String
  plantCount: Int
  metadata: String
  estimatedHarvestDateTime: Int
  actualHarvestDateTime: Int
  startDateTime: Int
  zoneId: Int
  strainId: Int

  zoneByZoneId: Zone

  enumByStrainId: Enum
}

type GrowthCyclesEdge {
  cursor: String

  node: GrowthCycle
}

enum GrowthCyclesOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  DESCRIPTION_ASC
  DESCRIPTION_DESC
  TITLE_ASC
  TITLE_DESC
  PLANT_COUNT_ASC
  PLANT_COUNT_DESC
  METADATA_ASC
  METADATA_DESC
  ESTIMATED_HARVEST_DATE_TIME_ASC
  ESTIMATED_HARVEST_DATE_TIME_DESC
  ACTUAL_HARVEST_DATE_TIME_ASC
  ACTUAL_HARVEST_DATE_TIME_DESC
  START_DATE_TIME_ASC
  START_DATE_TIME_DESC
  ZONE_ID_ASC
  ZONE_ID_DESC
  STRAIN_ID_ASC
  STRAIN_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input GrowthCycleCondition {
  id: Int

  description: String

  title: String

  plantCount: Int

  metadata: String

  estimatedHarvestDateTime: Int

  actualHarvestDateTime: Int

  startDateTime: Int

  zoneId: Int

  strainId: Int
}

type DeviceEnmosConnection {
  nodes: [DeviceEnmo]!

  edges: [DeviceEnmosEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type DeviceEnmosEdge {
  cursor: String

  node: DeviceEnmo
}

enum DeviceEnmosOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  DESCRIPTION_ASC
  DESCRIPTION_DESC
  STATUS_MESSAGE_ASC
  STATUS_MESSAGE_DESC
  DEVICE_CONFIG_ASC
  DEVICE_CONFIG_DESC
  ZONE_ID_ASC
  ZONE_ID_DESC
  STATUS_ID_ASC
  STATUS_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input DeviceEnmoCondition {
  id: Int

  description: String

  statusMessage: String

  deviceConfig: String

  zoneId: Int

  statusId: Int
}

enum DeviceTypesOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  DESCRIPTION_ASC
  DESCRIPTION_DESC
  STATUS_MESSAGE_ASC
  STATUS_MESSAGE_DESC
  DEVICE_CONFIG_ASC
  DEVICE_CONFIG_DESC
  DEFAULT_ADDRESS_ASC
  DEFAULT_ADDRESS_DESC
  ADDRESS_OPTIONS_ASC
  ADDRESS_OPTIONS_DESC
  DEVICE_ENMO_ID_ASC
  DEVICE_ENMO_ID_DESC
  TYPE_ID_ASC
  TYPE_ID_DESC
  UOM_ID_ASC
  UOM_ID_DESC
  MONITORING_PARAMS_ID_ASC
  MONITORING_PARAMS_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input DeviceTypeCondition {
  id: Int

  description: String

  statusMessage: String

  deviceConfig: String

  defaultAddress: String

  addressOptions: String

  deviceEnmoId: Int

  typeId: Int

  uomId: Int

  monitoringParamsId: Int
}

type MonitoringParam implements Node {
  nodeId: ID!
  id: Int!
  isEnabled: Boolean
  greenMaxDark: Float
  greenMinDark: Float
  greenMaxLight: Float
  greenMinLight: Float
  yellowMaxDark: Float
  yellowMinDark: Float
  yellowMaxLight: Float
  yellowMinLight: Float

  deviceTypesByMonitoringParamsId(
    first: Int

    last: Int

    offset: Int

    before: String

    after: String

    orderBy: [DeviceTypesOrderBy!] = [PRIMARY_KEY_ASC]

    condition: DeviceTypeCondition
  ): DeviceTypesConnection!
}

type DeviceTypesEdge {
  cursor: String

  node: DeviceType
}

type LocationEnmosConnection {
  nodes: [LocationEnmo]!

  edges: [LocationEnmosEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type LocationEnmosEdge {
  cursor: String

  node: LocationEnmo
}

enum LocationEnmosOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  DESCRIPTION_ASC
  DESCRIPTION_DESC
  METADATA_ASC
  METADATA_DESC
  NAME_ID_ASC
  NAME_ID_DESC
  ORGANIZATION_ID_ASC
  ORGANIZATION_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input LocationEnmoCondition {
  id: Int

  description: String

  metadata: String

  nameId: Int

  organizationId: Int
}

type ClassificationsEdge {
  cursor: String

  node: Classification
}

type DetectorsConnection {
  nodes: [Detector]!

  edges: [DetectorsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type DetectorsEdge {
  cursor: String

  node: Detector
}

enum DetectorsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  NAME_ASC
  NAME_DESC
  VERSION_ASC
  VERSION_DESC
  METADATA_ASC
  METADATA_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input DetectorCondition {
  id: Int

  name: String

  version: String

  metadata: String
}

type EnumsConnection {
  nodes: [Enum]!

  edges: [EnumsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type EnumsEdge {
  cursor: String

  node: Enum
}

enum EnumsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  VALUE_ASC
  VALUE_DESC
  TYPE_ASC
  TYPE_DESC
  CODE_ASC
  CODE_DESC
  METADATA_ASC
  METADATA_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input EnumCondition {
  id: Int

  value: String

  type: String

  code: String

  metadata: String
}

type MonitoringParamsConnection {
  nodes: [MonitoringParam]!

  edges: [MonitoringParamsEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type MonitoringParamsEdge {
  cursor: String

  node: MonitoringParam
}

enum MonitoringParamsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  IS_ENABLED_ASC
  IS_ENABLED_DESC
  GREEN_MAX_DARK_ASC
  GREEN_MAX_DARK_DESC
  GREEN_MIN_DARK_ASC
  GREEN_MIN_DARK_DESC
  GREEN_MAX_LIGHT_ASC
  GREEN_MAX_LIGHT_DESC
  GREEN_MIN_LIGHT_ASC
  GREEN_MIN_LIGHT_DESC
  YELLOW_MAX_DARK_ASC
  YELLOW_MAX_DARK_DESC
  YELLOW_MIN_DARK_ASC
  YELLOW_MIN_DARK_DESC
  YELLOW_MAX_LIGHT_ASC
  YELLOW_MAX_LIGHT_DESC
  YELLOW_MIN_LIGHT_ASC
  YELLOW_MIN_LIGHT_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input MonitoringParamCondition {
  id: Int

  isEnabled: Boolean

  greenMaxDark: Float

  greenMinDark: Float

  greenMaxLight: Float

  greenMinLight: Float

  yellowMaxDark: Float

  yellowMinDark: Float

  yellowMaxLight: Float

  yellowMinLight: Float
}

type UsersConnection {
  nodes: [User]!

  edges: [UsersEdge!]!

  pageInfo: PageInfo!

  totalCount: Int!
}

type UsersEdge {
  cursor: String

  node: User
}

enum UsersOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  NAME_ASC
  NAME_DESC
  EMAIL_ASC
  EMAIL_DESC
  PASSWORD_ASC
  PASSWORD_DESC
  METADATA_ASC
  METADATA_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

input UserCondition {
  id: Int

  name: String

  email: String

  password: String

  metadata: String
}