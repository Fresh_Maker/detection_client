'''
find pairs [x,y] where x-y=k

solve for y 
y = -(k-x) = x - k
brute o(n^2)
for
 for
  condition is met update return with that pair


preprocess set
for index, x in enum(arr)
  if set contains (x - k):
    result.append(x, y)
return result

t: n + n * 1
s: n + n
preprocess sort arr
'''

def find_pairs_with_given_difference2(arr, k):
  result = []
  for x in arr: # x = 0
    for y in arr:
      if x-y == k:
        result.append([x,y])
  return result

def find_pairs_with_given_difference(arr, k):
  # k = 1
  sett = set(arr)
  result = []
  for x in arr: # x = 0
    # 0 - 1 = -1 
    if (x - k) in sett:
      # result = [(0, -1)]
      result.append([x, x-k])
  return result
  
arr= [0, -1, -2, 2, 1]
k = 1
# expect 4 couples [[1, 0], [0, -1], [-1, -2], [2, 1]]

#arr = [1, 7, 5, 3, 32, 17, 12]
#k = 17
# expect []
print(find_pairs_with_given_difference(arr, k))
print(find_pairs_with_given_difference2(arr, k))